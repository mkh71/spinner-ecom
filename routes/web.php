<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('frontend.index');
// });
// //for frontend 
Route::get('/', [
	'uses' => 'Frontend\IndexController@index',
	'as' => '/'
]);

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix'=>'carts'],function(){
	Route::get('/',"Frontend\CartController@index")->name('cart_index');
	Route::get('/create',"Frontend\CartController@create")->name('cart_create');
	Route::post('/store',"Frontend\CartController@store")->name('cart_store');
	Route::get('/show/{id}',"Frontend\CartController@show")->name('cart_show');
	Route::get('/edit/{id}',"Frontend\CartController@edit")->name('cart_edit');
	Route::post('/update/{id}',"Frontend\CartController@update")->name('cart_update');
	Route::post('/delete/{id}',"Frontend\CartController@delete")->name('cart_delete');	
});

Route::group(['prefix'=>'customer_reviews'],function(){
		Route::get('/',"Frontend\ProductReviewController@index")->name('customer_review_index');
		Route::get('/create',"Frontend\ProductReviewController@create")->name('customer_review_create');
		Route::post('/store',"Frontend\ProductReviewController@store")->name('customer_review_store');
		Route::get('/show/{id}',"Frontend\ProductReviewController@show")->name('customer_review_show');
		Route::get('/edit/{id}',"Frontend\ProductReviewController@edit")->name('customer_review_edit');
		Route::post('/update/{id}',"Frontend\ProductReviewController@update")->name('customer_review_update');
		Route::post('/delete/{id}',"Frontend\ProductReviewController@delete")->name('customer_review_delete');
});

Route::group(['prefix'=>'checkouts'],function(){
		Route::get('/',"Frontend\CheckoutController@index")->name('checkout_index');
		Route::get('/create',"Frontend\CheckoutController@create")->name('checkout_create');
		Route::post('/store',"Frontend\CheckoutController@store")->name('checkout_store');
		Route::get('/show/{id}',"Frontend\CheckoutController@show")->name('checkout_show');
		Route::get('/edit/{id}',"Frontend\CheckoutController@edit")->name('checkout_edit');
		Route::post('/update/{id}',"Frontend\CheckoutController@update")->name('checkout_update');
		Route::post('/delete/{id}',"Frontend\CheckoutController@delete")->name('checkout_delete');
});


Route::group(['prefix'=>'shippings'],function(){
		Route::get('/',"Frontend\ShipmentController@index")->name('shipping_index');
		Route::get('/create',"Frontend\ShipmentController@create")->name('shipping_create');
		Route::post('/store',"Frontend\ShipmentController@store")->name('shipping_store');
		Route::get('/show/{id}',"Frontend\ShipmentController@show")->name('shipping_show');
		Route::get('/edit/{id}',"Frontend\ShipmentController@edit")->name('shipping_edit');
		Route::post('/update/{id}',"Frontend\ShipmentController@update")->name('shipping_update');
		Route::post('/delete/{id}',"Frontend\ShipmentController@delete")->name('shipping_delete');
});


Route::group(['prefix'=>'admin'],function(){
		Route::get('/', [
			'uses' => 'Backend\Admin\DashboardController@dashboard',
			'as' => 'admin.dashboard'
		]);
	    //admin login and logout
		Route::get('/login',[
			'uses'=>'Auth\Admin\LoginController@showLoginForm',
			'as'=>'admin.login'
		]);
		Route::post('/login/submit',[
			'uses'=>'Auth\Admin\LoginController@login',
			'as'=>'admin.login.submit'
		]);
		Route::post('/logout',[
			'uses'=>'Auth\Admin\LoginController@logout',
			'as'=>'admin.logout'
	    ]);
	    //admin Forgotpassword and reset password
		Route::post('/password/email',[
			'uses'=>'Auth\Admin\ForgotPasswordController@sendResetLinkEmail',
			'as'=>'admin.password.email'
		]);
		Route::get('/password/reset',[
			'uses'=>'Auth\Admin\ForgotPasswordController@showLinkRequestForm',
			'as'=>'admin.password.request'
		]);
		Route::get('/password/reset/{token}',[
			'uses'=>'Auth\Admin\ResetPasswordController@showResetForm',
			'as'=>'admin.password.reset'
		]);
		Route::post('/password/reset',[
			'uses'=>'Auth\Admin\ResetPasswordController@reset',
			'as'=>'admin.password.reset.post'
		]);


		Route::group(['prefix'=>'brands'],function(){
			Route::get('/',"Backend\Admin\BrandController@index")->name('brand_index');
			Route::get('/create',"Backend\Admin\BrandController@create")->name('brand_create');
			Route::post('/store',"Backend\Admin\BrandController@store")->name('brand_store');
			Route::get('/show/{id}',"Backend\Admin\BrandController@show")->name('brand_show');
			Route::get('/edit/{id}',"Backend\Admin\BrandController@edit")->name('brand_edit');
			Route::post('/update/{id}',"Backend\Admin\BrandController@update")->name('brand_update');
			Route::post('/delete/{id}',"Backend\Admin\BrandController@delete")->name('brand_delete');
			Route::post('/store',"Backend\Admin\BrandController@store")->name('brand_store');
			
		});


		Route::group(['prefix'=>'categories'],function(){
			Route::get('/',"Backend\Admin\CategoryController@index")->name('category_index');
			Route::get('/create',"Backend\Admin\CategoryController@create")->name('category_create');
			Route::post('/store',"Backend\Admin\CategoryController@store")->name('category_store');
			Route::get('/show/{id}',"Backend\Admin\CategoryController@show")->name('category_show');
			Route::get('/edit/{id}',"Backend\Admin\CategoryController@edit")->name('category_edit');
			Route::post('/update/{id}',"Backend\Admin\CategoryController@update")->name('category_update');
			Route::post('/delete/{id}',"Backend\Admin\CategoryController@delete")->name('category_delete');
			Route::post('/store',"Backend\Admin\CategoryController@store")->name('category_store');
			
		});

		Route::group(['prefix'=>'products'],function(){
			Route::get('/',"Backend\Admin\ProductController@index")->name('product_index');
			Route::get('/create',"Backend\Admin\ProductController@create")->name('product_create');
			Route::post('/store',"Backend\Admin\ProductController@store")->name('product_store');
			Route::get('/show/{id}',"Backend\Admin\ProductController@show")->name('product_show');
			Route::get('/edit/{id}',"Backend\Admin\ProductController@edit")->name('product_edit');
			Route::post('/update/{id}',"Backend\Admin\ProductController@update")->name('product_update');
			Route::post('/delete/{id}',"Backend\Admin\ProductController@delete")->name('product_delete');
			Route::post('/store',"Backend\Admin\ProductController@store")->name('product_store');	
		});

		Route::group(['prefix'=>'colors'],function(){
			Route::get('/',"Backend\Admin\ColorController@index")->name('color_index');
			Route::get('/create',"Backend\Admin\ColorController@create")->name('color_create');
			Route::post('/store',"Backend\Admin\ColorController@store")->name('color_store');
			Route::get('/show/{id}',"Backend\Admin\ColorController@show")->name('color_show');
			Route::get('/edit/{id}',"Backend\Admin\ColorController@edit")->name('color_edit');
			Route::post('/update/{id}',"Backend\Admin\ColorController@update")->name('color_update');
			Route::post('/delete/{id}',"Backend\Admin\ColorController@delete")->name('color_delete');
			Route::post('/store',"Backend\Admin\ColorController@store")->name('color_store');	
		});

		Route::group(['prefix'=>'reviews'],function(){
			Route::get('/',"Backend\Admin\ReviewController@index")->name('review_index');
			Route::get('/create',"Backend\Admin\ReviewController@create")->name('review_create');
			Route::post('/store',"Backend\Admin\ReviewController@store")->name('review_store');
			Route::get('/show/{id}',"Backend\Admin\ReviewController@show")->name('review_show');
			Route::get('/edit/{id}',"Backend\Admin\ReviewController@edit")->name('review_edit');
			Route::post('/update/{id}',"Backend\Admin\ReviewController@update")->name('review_update');
			Route::post('/delete/{id}',"Backend\Admin\ReviewController@delete")->name('review_delete');
			Route::post('/store',"Backend\Admin\ReviewController@store")->name('review_store');	
		});
		
		Route::group(['prefix'=>'deliveries'],function(){
			Route::get('/',"Backend\Admin\DeliveryController@index")->name('delivery_index');
			Route::get('/create',"Backend\Admin\DeliveryController@create")->name('delivery_create');
			Route::post('/store',"Backend\Admin\DeliveryController@store")->name('delivery_store');
			Route::get('/show/{id}',"Backend\Admin\DeliveryController@show")->name('delivery_show');
			Route::get('/edit/{id}',"Backend\Admin\DeliveryController@edit")->name('delivery_edit');
			Route::post('/update/{id}',"Backend\Admin\DeliveryController@update")->name('delivery_update');
			Route::post('/delete/{id}',"Backend\Admin\DeliveryController@delete")->name('delivery_delete');
			Route::post('/store',"Backend\Admin\DeliveryController@store")->name('delivery_store');	
		});

		Route::group(['prefix'=>'discounts'],function(){
			Route::get('/',"Backend\Admin\DiscountController@index")->name('discount_index');
			Route::get('/create',"Backend\Admin\DiscountController@create")->name('discount_create');
			Route::post('/store',"Backend\Admin\DiscountController@store")->name('discount_store');
			Route::get('/show/{id}',"Backend\Admin\DiscountController@show")->name('discount_show');
			Route::get('/edit/{id}',"Backend\Admin\DiscountController@edit")->name('discount_edit');
			Route::post('/update/{id}',"Backend\Admin\DiscountController@update")->name('discount_update');
			Route::post('/delete/{id}',"Backend\Admin\DiscountController@delete")->name('discount_delete');
			Route::post('/store',"Backend\Admin\DiscountController@store")->name('discount_store');	
		});

		Route::group(['prefix'=>'shipments'],function(){
			Route::get('/',"Backend\Admin\ShipmentController@index")->name('shipment_index');
			Route::get('/create',"Backend\Admin\ShipmentController@create")->name('shipment_create');
			Route::post('/store',"Backend\Admin\ShipmentController@store")->name('shipment_store');
			Route::get('/show/{id}',"Backend\Admin\ShipmentController@show")->name('shipment_show');
			Route::get('/edit/{id}',"Backend\Admin\ShipmentController@edit")->name('shipment_edit');
			Route::post('/update/{id}',"Backend\Admin\ShipmentController@update")->name('shipment_update');
			Route::post('/delete/{id}',"Backend\Admin\ShipmentController@delete")->name('shipment_delete');
			Route::post('/store',"Backend\Admin\ShipmentController@store")->name('shipment_store');	
		});

		Route::group(['prefix'=>'cartridges'],function(){
			Route::get('/',"Backend\Admin\CartridgeController@index")->name('cartridge_index');
			Route::get('/create',"Backend\Admin\CartridgeController@create")->name('cartridge_create');
			Route::post('/store',"Backend\Admin\CartridgeController@store")->name('cartridge_store');
			Route::get('/show/{id}',"Backend\Admin\CartridgeController@show")->name('cartridge_show');
			Route::get('/edit/{id}',"Backend\Admin\CartridgeController@edit")->name('cartridge_edit');
			Route::post('/update/{id}',"Backend\Admin\CartridgeController@update")->name('cartridge_update');
			Route::post('/delete/{id}',"Backend\Admin\CartridgeController@delete")->name('cartridge_delete');	
		});

});