<section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ asset('/') }}assets/admin/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Alexander Pierce</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Brand</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class=""><a href="{{ route('brand_index') }}"><i class="fa fa-circle-o"></i>All Brand List</a></li>
            <li><a href="{{ route('brand_create')}}"><i class="fa fa-circle-o"></i>Add Brand</a></li>
          </ul>
        </li>
        <li class="treeview">
        <a href="#">
            <i class="fa fa-dashboard"></i> <span>Category</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ route('category_index')}}"><i class="fa fa-circle-o"></i> All Category List</a></li>
            <li><a href="{{ route('category_create')}}"><i class="fa fa-circle-o"></i> Add Category</a></li>
          </ul>
        </li>

        <li class="treeview">
        <a href="#">
            <i class="fa fa-dashboard"></i> <span>Color</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ route('color_index')}}"><i class="fa fa-circle-o"></i> All Color List</a></li>
            <li><a href="{{ route('color_create')}}"><i class="fa fa-circle-o"></i> Add Color</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Cartridge</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ route('cartridge_index')}}"><i class="fa fa-circle-o"></i> All Cartridge List</a></li>
            <li><a href="{{ route('cartridge_create')}}"><i class="fa fa-circle-o"></i> Add Cartridge</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Product</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ route('product_index')}}"><i class="fa fa-circle-o"></i> All Product List</a></li>
            <li><a href="{{ route('product_create')}}"><i class="fa fa-circle-o"></i> Add Product</a></li>
          </ul>
        </li>

        <li class="treeview">
        <a href="#">
            <i class="fa fa-dashboard"></i> <span>Product</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ route('product_index')}}"><i class="fa fa-circle-o"></i> All Product List</a></li>
            <li><a href="{{ route('product_create')}}"><i class="fa fa-circle-o"></i> Add Product</a></li>
          </ul>
        </li>
      
        <li class="treeview">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Discount</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ route('discount_index')}}"><i class="fa fa-circle-o"></i> ALL Discount</a></li>
            <li><a href="{{ route('discount_create')}}"><i class="fa fa-circle-o"></i> Add Discount</a></li>
          
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-laptop"></i>
            <span>Shipment</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ route('shipment_index')}}"><i class="fa fa-circle-o"></i>All Shipment Shipment </a></li>
            <li><a href="{{ route('shipment_index')}}"><i class="fa fa-circle-o"></i>Add Shipment</a></li>
           
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Delivery</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ route('delivery_index')}}"><i class="fa fa-circle-o"></i>All Delivery List</a></li>
            <li><a href="{{ route('delivery_create')}}"><i class="fa fa-circle-o"></i>Add Delivery</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-table"></i> <span>Order</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="pages/tables/simple.html"><i class="fa fa-circle-o"></i>All Order List</a></li>
            <li><a href="pages/tables/data.html"><i class="fa fa-circle-o"></i></a></li>
          </ul>
        </li>
      
        <li class="header">LABELS</li>
        <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>
      </ul>
    </section>