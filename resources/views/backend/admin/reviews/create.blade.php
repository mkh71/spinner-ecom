@extends('Backend.admin.layouts.master')

@section('content')
<section class="content-header">
      <h1>
        Add review 
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('review_create') }}" class="btn btn-success"> All review List</a></li>
      </ol>
    </section>

 <section class="content" style="margin-top: 10px;">
      <div class="row">
        <div class="col-lg-8 col-xs-8 col-sm-offset-2">
        	<div class="box">
        		<div class="box-header">
        			<h4>Review Create</h4>
        		</div>
        		<div class="box-body">
        			<form method="POST" class="user" action="{{ route('review_store') }}" enctype="multipart/form-data">
          @csrf

          <div class="form-group row">
              <label for="star" class="col-sm-3">Start</label>
              <div class="form-input col-sm-9">
                  <input type="text" class="form-control form-control-user is-valid form-control-sm" name="star" id="star" placeholder="star" value="{{old('star')}}" required>
                  <div class="valid-feedback">
                    {{ ($errors->has('star')) ? $errors->first('star') : ''}}
                  </div>
              </div>
          </div>

          <div class="form-group row">
              <label for="parcentage" class="col-sm-3">Review Percentage</label>
              <div class="form-input col-sm-9">
                  <input type="text" class="form-control form-control-user is-valid form-control-sm" name="parcentage" id="parcentage" placeholder="parcentage" value="{{old('parcentage')}}" required>
                  <div class="valid-feedback">
                    {{ ($errors->has('parcentage')) ? $errors->first('parcentage') : ''}}
                  </div>
              </div>
          </div>

         


          <button class="btn btn-primary" type="submit">Add review</button>
      </form>
        		</div>
        	</div>
        </div>
      </div>
    </section>
@endsection