@extends('Backend.admin.layouts.master')

@section('content')
<section class="content-header">
      <h1>
        ALL review List
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('review_create') }}" class="btn btn-success">  Add review</a></li>
        
      </ol>
    </section>

 <section class="content" style="margin-top: 10px;">
      <div class="row">
        <div class="col-lg-12 col-xs-12">
        	<div class="box">
        		<div class="box-header">
        			
        		</div>
        		<div class="box-body">
        			<div class="table-responsive mt-2">
        <table id="reviews" class="table table-bordered table-striped">
          <caption>List of reviews</caption>
          <thead>
  					<tr>
  						<th>SL</th>
  						<th>Star</th>
  						<th>Parcentage</th>
  						<th>Action</th>
  					</tr>
  				</thead>
  				<tbody>
  					<tr>
  						<div style="display: none;">{{$a=1}}</div>
  						@foreach($reviews as $review)
  						<td>{{ $a++ }}</td>
  						<td>{{ $review->star }}</td>
  						</td>
  						<td>
  						    @if($review->parcentage)
		                  <p>{{ $review->parcentage}}%</p>
		                  @else
		                    <p>N/A</p>
		                  @endif
  						</td>
            
  						<td>
  							<a href="{{route('review_edit', $review->id)}}" class="badge badge-warning">Edit</a>
                <a href="#DeleteModal{{ $review->id}}" data-toggle="modal" class="badge badge-danger btn-sm">Delete</a>
								<div class="modal fade" id="DeleteModal{{$review->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title" id="exampleModalLabel">Are You Sure To Delete!</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">
												<form action="{{ route('review_delete', $review->id)}}" method="POST">
													{{csrf_field()}}
												<button type="submit" class="badge badge-success">Delete</button>
												</form>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
											</div>
										</div>
									</div>
								</div>
							</a>
  						</td>
  					</tr>
  					@endforeach
  				</tbody>
        </table>
      </div>
        		</div>
        	</div>
        </div>
      </div>
    </section>
@endsection