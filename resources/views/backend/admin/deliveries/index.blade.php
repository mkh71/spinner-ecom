@extends('Backend.admin.layouts.master')

@section('content')
<section class="content-header">
      <h1>
        ALL delivery List
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('delivery_create') }}" class="btn btn-success">  Add delivery</a></li>
        
      </ol>
    </section>

 <section class="content" style="margin-top: 10px;">
      <div class="row">
        <div class="col-lg-12 col-xs-12">
        	<div class="box">
        		<div class="box-header">
        			
        		</div>
        		<div class="box-body">
        			<div class="table-responsive mt-2">
        <table id="deliveries" class="table table-bordered table-striped">
          <caption>List of deliveries</caption>
          <thead>
  					<tr>
  						<th>SL</th>
  						<th>Name</th>
  						<th>Amount</th>
  						<th>Description</th>
  						<th>Action</th>
  					</tr>
  				</thead>
  				<tbody>
  					<tr>
  						<div style="display: none;">{{$a=1}}</div>
  						@foreach($deliveries as $delivery)
  						<td>{{ $a++ }}</td>
  						<td>{{ $delivery->delivery_name }}</td>
  						<td>{{ $delivery->delivery_amount }}%</td>
  						<td>
  						    @if($delivery->delivery_description)
		                  <p>{{ $delivery->delivery_description}}</p>
		                  @else
		                    <p>N/A</p>
		                  @endif
  						</td>
            
  						<td>
  							<a href="{{route('delivery_edit', $delivery->id)}}" class="badge badge-warning">Edit</a>
                <a href="#DeleteModal{{ $delivery->id}}" data-toggle="modal" class="badge badge-danger btn-sm">Delete</a>
								<div class="modal fade" id="DeleteModal{{$delivery->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title" id="exampleModalLabel">Are You Sure To Delete!</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">
												<form action="{{ route('delivery_delete', $delivery->id)}}" method="POST">
													{{csrf_field()}}
												<button type="submit" class="badge badge-success">Delete</button>
												</form>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
											</div>
										</div>
									</div>
								</div>
							</a>
  						</td>
  					</tr>
  					@endforeach
  				</tbody>
        </table>
      </div>
        		</div>
        	</div>
        </div>
      </div>
    </section>
@endsection