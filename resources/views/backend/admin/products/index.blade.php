@extends('Backend.admin.layouts.master')

@section('content')
<section class="content-header">
      <h1>
        ALL product List
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('product_create') }}" class="btn btn-success">  Add product</a></li>
        
      </ol>
    </section>

 <section class="content" style="margin-top: 10px;">
      <div class="row">
        <div class="col-lg-12 col-xs-12">
        	<div class="box">
        		<div class="box-header">
        			
        		</div>
        		<div class="box-body">
        			<div class="table-responsive mt-2">
        <table id="products" class="table table-bordered table-striped">
          <caption>List of products</caption>
          <thead>
  					<tr>
  						<th>SL</th>
              <th>Cartridge No</th>
              <th>Title</th>
              <th>Brand</th>
              <th>Category</th>
              <th>Price</th>
              <th>Quantity</th>
              <th>Offer</th>
  						<th>Color</th>
  						<th>Image</th>
  						<th>Description</th>
  						<th>Action</th>
  					</tr>
  				</thead>
  				<tbody>
  					<tr>
  						<div style="display: none;">{{$a=1}}</div>
  						@foreach($products as $product)
  						<td>{{ $a++ }}</td>
              <td>{{ $product->cartridge->cartridge_no }}</td>
              <td>{{ $product->product_title }}</td>
              <td>{{ $product->brand->brand_name }}</td>
  						<td>{{ $product->category->category_name }}</td>
              <td>{{ $product->cartridge->price }}</td>
              <td>{{ $product->product_quantity }}</td>
              <td>{{ $product->product_offer_price }}</td>
              <td>
               {{ $product->cartridge->color->color_name }}
              </td>
                        
  						<td>
              
                @foreach($product->images as $product_image)
                 @endforeach
                  <p>
                      <img class="" src="{{asset('assets/admin/images/products/'.$product_image->product_image)}}" height="50px" width="200px">
                    </p> 
                 
  						</td>
  						<td>
  						    @if($product->product_description)
		                  <p>{{ $product->product_description}}</p>
		                  @else
		                    <p>N/A</p>
		                  @endif
  						</td>
            
  						<td>
  							<a href="{{route('product_edit', $product->id)}}" class="badge badge-warning">Edit</a>
                <a href="#DeleteModal{{ $product->id}}" data-toggle="modal" class="badge badge-danger btn-sm">Delete</a>
								<div class="modal fade" id="DeleteModal{{$product->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title" id="exampleModalLabel">Are You Sure To Delete!</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">
												<form action="{{ route('product_delete', $product->id)}}" method="POST">
													{{csrf_field()}}
												<button type="submit" class="badge badge-success">Delete</button>
												</form>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
											</div>
										</div>
									</div>
								</div>
							</a>
  						</td>
  					</tr>
  					@endforeach
  				</tbody>
        </table>
      </div>
        		</div>
        	</div>
        </div>
      </div>
    </section>
@endsection