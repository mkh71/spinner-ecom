@extends('Backend.admin.layouts.master')

@section('content')
<section class="content-header">
      <h1>
        Add Product 
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('product_index') }}" class="btn btn-success"> All Product List</a></li>
      </ol>
    </section>

 <section class="content" style="margin-top: 10px;">
      <div class="row">
        <div class="col-lg-8 col-xs-8 col-sm-offset-2">
        	<div class="box">
        		<div class="box-header">
        			<h4>Product Create</h4>
        		</div>
        		<div class="box-body">
        			<form method="POST" class="user" action="{{ route('product_store') }}" enctype="multipart/form-data">
          @csrf
                <div class="form-group row">
                    <label for="cartridge_id" class="col-sm-3">Cartridge No</label>
                    <div class="form-input col-sm-9">
                        <select name="cartridge_id" id="cartridge_id" class="form-control">
                            <option value="0" disabled="true" selected="true">===Select Cartridge===</option>
                            @foreach($cartridges as $cartridge)
                                <option value="{{$cartridge->id }}">{{$cartridge->cartridge_no}}</option>
                            @endforeach
                        </select>
                        <div class="valid-feedback">
                            {{ ($errors->has('cartridge_id')) ? $errors->first('cartridge_id') : ''}}
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                <label for="category_id" class="col-sm-3">Category</label>
                <div class="form-input col-sm-9">
                    <select name="category_id" id="category_id" class="form-control">
                            <option value="0" disabled="true" selected="true">===Select Category===</option>
                            @foreach($categories as $category)
                            <option value="{{$category->id }}">{{$category->category_name}}</option>
                            @endforeach
                    </select>
                    <div class="valid-feedback">
                      {{ ($errors->has('category_id')) ? $errors->first('category_id') : ''}}
                    </div>
                </div>
            </div>

            <div class="form-group row">
                <label for="brand_id" class="col-sm-3">Brand</label>
                <div class="form-input col-sm-9">
                    <select name="brand_id" id="brand_id" class="form-control">
                            <option value="0" disabled="true" selected="true">===Select Brand===</option>
                            @foreach($brands as $brand)
                            <option value="{{$brand->id}}">{{$brand->brand_name}}</option>
                            @endforeach
                    </select>
                    <div class="valid-feedback">
                      {{ ($errors->has('brand_id')) ? $errors->first('brand_id') : ''}}
                    </div>
                </div>
            </div>

          <div class="form-group row">
              <label for="product_title" class="col-sm-3">Product Name</label>
              <div class="form-input col-sm-9">
                  <input type="text" class="form-control form-control-user is-valid form-control-sm" name="product_title" id="product_title" placeholder="Enter Category product_title" value="{{old('product_title')}}" required>
                  <div class="valid-feedback">
                    {{ ($errors->has('product_title')) ? $errors->first('product_title') : ''}}
                  </div>
              </div>
          </div>

          <div class="form-group row">
              <label for="product_quantity" class="col-sm-3">Product Quantity</label>
              <div class="form-input col-sm-9">
                  <input type="number" class="form-control form-control-user is-valid form-control-sm" name="product_quantity" id="product_quantity" placeholder="Enter Category product_quantity" value="{{old('product_quantity')}}" required>
                  <div class="valid-feedback">
                    {{ ($errors->has('product_quantity')) ? $errors->first('product_quantity') : ''}}
                  </div>
              </div>
          </div>
          

          <div class="form-group row">
              <label for="product_offer_price" class="col-sm-3">Product Offer Price</label>
              <div class="form-input col-sm-9">
                  <input type="number" class="form-control form-control-user is-valid form-control-sm" name="product_offer_price" id="product_offer_price" placeholder="Enter Category product_offer_price" value="{{old('product_offer_price')}}" required>
                  <div class="valid-feedback">
                    {{ ($errors->has('product_offer_price')) ? $errors->first('product_offer_price') : ''}}
                  </div>
              </div>
          </div>

          <div class="form-group row">
              <label for="product_description" class="col-sm-3">Product Description</label>
              <div class="form-input col-sm-9">
                  <textarea name="product_description" cols="4" rows="5" value="{{old('product_description')}}" class="form-control form-control-user is-valid form-control-sm input-md" id="product_description" required>{{ old('product_description')}}</textarea>
                  <div class="valid-feedback">
                    {{ ($errors->has('product_description')) ? $errors->first('product_description') : ''}}
                  </div>
              </div>
          </div>


          <div class="form-group row">
              <label for="product_images[]" class="col-sm-3">Product Images</label>
              <div class="form-input col-sm-9">
                  <input type="file" class="form-control form-control-user is-valid form-control-sm" name="product_images[]" id="product_images[]" placeholder="Enter Ward Bangla Name" value="" multiple>
                  <div class="valid-feedback">
                    {{ ($errors->has('product_images[]')) ? $errors->first('product_images[]') : ''}}
                  </div>
              </div>
          </div>


          <button class="btn btn-primary" type="submit">Add Product</button>
      </form>
        		</div>
        	</div>
        </div>
      </div>
    </section>
@endsection