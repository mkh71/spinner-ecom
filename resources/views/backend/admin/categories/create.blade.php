@extends('Backend.admin.layouts.master')

@section('content')
<section class="content-header">
      <h1>
        Add Brand 
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('category_index') }}" class="btn btn-success"> All Brand List</a></li>
      </ol>
    </section>

 <section class="content" style="margin-top: 10px;">
      <div class="row">
        <div class="col-lg-8 col-xs-8 col-sm-offset-2">
        	<div class="box">
        		<div class="box-header">
        			<h4>Barnd Create</h4>
        		</div>
        		<div class="box-body">
        			<form method="POST" class="user" action="{{ route('category_store') }}" enctype="multipart/form-data">
          @csrf

          <div class="form-group row">
              <label for="category_name" class="col-sm-3">Category Name</label>
              <div class="form-input col-sm-9">
                  <input type="text" class="form-control form-control-user is-valid form-control-sm" name="category_name" id="category_name" placeholder="Enter Category category_name" value="{{old('category_name')}}" required>
                  <div class="valid-feedback">
                    {{ ($errors->has('category_name')) ? $errors->first('category_name') : ''}}
                  </div>
              </div>
          </div>

          <div class="form-group row">
              <label for="category_description" class="col-sm-3">Category Description</label>
              <div class="form-input col-sm-9">
                  <textarea name="category_description" cols="4" rows="5" value="{{old('category_description')}}" class="form-control form-control-user is-valid form-control-sm input-md" id="category_description" required>{{ old('category_description')}}</textarea>
                  <div class="valid-feedback">
                    {{ ($errors->has('category_description')) ? $errors->first('category_description') : ''}}
                  </div>
              </div>
          </div>


          <div class="form-group row">
              <label for="category_image" class="col-sm-3">Category category_Image</label>
              <div class="form-input col-sm-9">
                  <input type="file" class="form-control form-control-user is-valid form-control-sm" name="category_image" id="category_image" placeholder="Enter Ward Bangla Name" value="">
                  <div class="valid-feedback">
                    {{ ($errors->has('category_image')) ? $errors->first('category_image') : ''}}
                  </div>
              </div>
          </div>


          <button class="btn btn-primary" type="submit">Create</button>
      </form>
        		</div>
        	</div>
        </div>
      </div>
    </section>
@endsection