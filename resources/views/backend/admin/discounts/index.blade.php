@extends('Backend.admin.layouts.master')

@section('content')
<section class="content-header">
      <h1>
        ALL discount List
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('discount_create') }}" class="btn btn-success">  Add discount</a></li>
        
      </ol>
    </section>

 <section class="content" style="margin-top: 10px;">
      <div class="row">
        <div class="col-lg-12 col-xs-12">
        	<div class="box">
        		<div class="box-header">
        			
        		</div>
        		<div class="box-body">
        			<div class="table-responsive mt-2">
        <table id="discounts" class="table table-bordered table-striped">
          <caption>List of discounts</caption>
          <thead>
  					<tr>
  						<th>SL</th>
  						<th>Code</th>
  						<th>Type</th>
  						<th>Amount</th>
  						<th>Expire Date</th>
  						<th>Product Name</th>
  						<th>Description</th>
  						<th>Action</th>
  					</tr>
  				</thead>
  				<tbody>
  					<tr>
  						<div style="display: none;">{{$a=1}}</div>
  						@foreach($discounts as $discount)
  						<td>{{ $a++ }}</td>
  						<td>{{ $discount->discount_code }}</td>
  						<td>{{ $discount->discount_type }}</td>
  						<td>{{ $discount->discount_amount }}</td>
  						<td>{{ $discount->discount_expire_date }}</td>
  						<td>{{ $discount->product->product_title }}</td>
  						
  						<td>
  						    @if($discount->discount_description)
		                  <p>{{ $discount->discount_description}}</p>
		                  @else
		                    <p>N/A</p>
		                  @endif
  						</td>
            
  						<td>
  							<a href="{{route('discount_edit', $discount->id)}}" class="badge badge-warning">Edit</a>
                <a href="#DeleteModal{{ $discount->id}}" data-toggle="modal" class="badge badge-danger btn-sm">Delete</a>
								<div class="modal fade" id="DeleteModal{{$discount->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title" id="exampleModalLabel">Are You Sure To Delete!</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">
												<form action="{{ route('discount_delete', $discount->id)}}" method="POST">
													{{csrf_field()}}
												<button type="submit" class="badge badge-success">Delete</button>
												</form>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
											</div>
										</div>
									</div>
								</div>
							</a>
  						</td>
  					</tr>
  					@endforeach
  				</tbody>
        </table>
      </div>
        		</div>
        	</div>
        </div>
      </div>
    </section>
@endsection