@extends('Backend.admin.layouts.master')

@section('content')
<section class="content-header">
      <h1>
        Add Color 
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('color_create') }}" class="btn btn-success"> All Color List</a></li>
      </ol>
    </section>

 <section class="content" style="margin-top: 10px;">
      <div class="row">
        <div class="col-lg-8 col-xs-8 col-sm-offset-2">
        	<div class="box">
        		<div class="box-header">
        			<h4>Barnd Create</h4>
        		</div>
        		<div class="box-body">
        			<form method="POST" class="user" action="{{ route('color_store') }}" enctype="multipart/form-data">
          @csrf

          <div class="form-group row">
              <label for="color_name" class="col-sm-3">Category Name</label>
              <div class="form-input col-sm-9">
                  <input type="text" class="form-control form-control-user is-valid form-control-sm" name="color_name" id="color_name" placeholder="Enter Category color_name" value="{{old('color_name')}}" required>
                  <div class="valid-feedback">
                    {{ ($errors->has('color_name')) ? $errors->first('color_name') : ''}}
                  </div>
              </div>
          </div>

         


          <button class="btn btn-primary" type="submit">Add Color</button>
      </form>
        		</div>
        	</div>
        </div>
      </div>
    </section>
@endsection