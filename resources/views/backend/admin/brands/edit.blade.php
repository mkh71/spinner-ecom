@extends('Backend.admin.layouts.master')

@section('content')
<section class="content-header">
      <h1>
        Add Brand 
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('brand_index') }}" class="btn btn-success"> All Brand List</a></li>
      </ol>
    </section>

 <section class="content" style="margin-top: 10px;">
      <div class="row">
        <div class="col-lg-8 col-xs-8">
        	<div class="box">
        		<div class="box-header">
        			<h4>Barnd Create</h4>
        		</div>
        		<div class="box-body">
        			<form method="POST" class="user" action="{{ route('brand_update',$brand->id) }}" enctype="multipart/form-data">
          @csrf

          <div class="form-group row">
              <label for="brand_name" class="col-sm-3">Category Name</label>
              <div class="form-input col-sm-9">
                  <input type="text" class="form-control form-control-user is-valid form-control-sm" name="brand_name" id="brand_name" placeholder="Enter Category brand_name" value="{{ $brand->brand_name}}" required>
                  <div class="valid-feedback">
                    {{ ($errors->has('brand_name')) ? $errors->first('brand_name') : ''}}
                  </div>
              </div>
          </div>

          <div class="form-group row">
              <label for="brand_description" class="col-sm-3">Category Description</label>
              <div class="form-input col-sm-9">
                  <textarea name="brand_description" cols="4" rows="5" value="{{old('brand_description')}}" class="form-control form-control-user is-valid form-control-sm input-md" id="brand_description" required>{{ $brand->brand_description}}</textarea>
                  <div class="valid-feedback">
                    {{ ($errors->has('brand_description')) ? $errors->first('brand_description') : ''}}
                  </div>
              </div>
          </div>


          <div class="form-group row">
              <label for="brand_image" class="col-sm-3">Category brand_Image</label>
              <div class="form-input col-sm-9">
                  <input type="file" class="form-control form-control-user is-valid form-control-sm" name="brand_image" id="brand_image" placeholder="Enter Ward Bangla Name" value="">
                  <div class="valid-feedback">
                    {{ ($errors->has('brand_image')) ? $errors->first('brand_image') : ''}}
                  </div>
              </div>
          </div>


          <button class="btn btn-primary" type="submit">Update Brand</button>
      </form>
        		</div>
        	</div>
        </div>
         <div class="col-lg-4 col-xs-4">

		      @if($brand->brand_image)
          <p>
            <img class="" src="{{asset('assets/admin/images/brands/'.$brand->brand_image)}}" height="200px" width="100%">
          </p>
          @else
            <p>N/A</p>
          @endif
         </div>
      </div>
    </section>
@endsection