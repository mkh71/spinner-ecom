<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
   public function images()
   {
   		return $this->hasMany(ProductImage::class);
   }

    public function cartridge(){
        return $this->belongsTo(Cartridge::class);
    }
   public function colors()
   {
   		return $this->hasMany(ProductColor::class);
   }

   public function brand(){
	    return $this->belongsTo(Brand::class);
	}

	public function category(){
	    return $this->belongsTo(Category::class);
	}

  

}
