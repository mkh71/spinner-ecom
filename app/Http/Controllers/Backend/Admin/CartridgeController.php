<?php

namespace App\Http\Controllers\Backend\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Cartridge;
use App\Models\Color;

class CartridgeController extends Controller
{
    
    public function index()
	{
	    $cartridges =Cartridge::orderBy('id','desc')->get();
	    return view('backend.admin.cartridges.index',compact('cartridges'));
	}

	public function create()
	{
		$colors =Color::orderBy('id','desc')->get();
	    return view('backend.admin.cartridges.create',[
	    	'colors'=>$colors
	    ]);
	}

	public function store(Request $request)
	{
		$this->validate($request,[
			'cartridge_no' => 'required|unique:cartridges',
			'price'=>'required',
			'color_id'=>'required',
			
		]);

		$cartridge=new Cartridge();
		$cartridge->cartridge_no=$request->cartridge_no;
		$cartridge->color_id=$request->color_id;
		$cartridge->other_code=$request->other_code;
		$cartridge->yield=$request->yield;
		$cartridge->price=$request->price;
		$cartridge->save();
		if(!is_null($cartridge)){
			session()->flash('success','cartridge create Successfully!!');
			return redirect()->route('cartridge_index');
		}else{
			session()->flash('stickly_error','Some Error Occer!!');
			return back();
		}
	}


	public function show($id)
	{
		$cartridge=Cartridge::findOrFail($id);
		return view('backend.admin.cartridges.show',compact('cartridge'));
	}


	public function edit($id)
	{
		$colors =Color::orderBy('id','desc')->get();
		$cartridge = Cartridge::findOrFail($id);
		if(!is_null($cartridge))
		{
			return view('backend.admin.cartridges.edit',[
				'cartridge'=>$cartridge,
				'colors'=>$colors,
			]);
		}else
		{
			return redirect()->route('cartridge_index');
		}
	}

	public function update(Request $request, $id)
	{
		$cartridge=cartridge::findOrFail($id);
		$cartridge->cartridge_no=$request->cartridge_no;
		$cartridge->color_id=$request->color_id;
		$cartridge->other_code=$request->other_code;
		$cartridge->yield=$request->yield;
		$cartridge->price=$request->price;
		$cartridge->update();
		if(!is_null($cartridge)){
			session()->flash('success','Cartridge Update Successfully!!');
			return redirect()->route('cartridge_index');
		}else{
			session()->flash('stickly_error','Some Error Occer!!');
			return back();
		}
	}
}
