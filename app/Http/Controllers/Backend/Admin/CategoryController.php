<?php

namespace App\Http\Controllers\Backend\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\MOdels\Category;
use Image;
use File;

class CategoryController extends Controller
{
    public function index()
	{
	    $categories =category::orderBy('id','desc')->get();
	    return view('backend.admin.categories.index',compact('categories'));
	}

	public function create()
	{
	    return view('backend.admin.categories.create');
	}

	public function store(Request $request)
	{
		$this->validate($request,[
			'category_name'=>'required',
			'category_description'=>'required',
			'category_image'=>'required'
			
		]);

		$category=new Category();
		$category->category_name=$request->category_name;
		$category->category_description=$request->category_description;
		if($request->file('category_image'))
        {
        	$images=$request->file('category_image');
            $img=$request->category_name.'.'.$images->getClientOriginalExtension();
            $location=public_path('assets/admin/images/categories/'.$img);
            Image::make($images)->save($location)->resize(300,300);
            $category->category_image=$img;
        }
		$category->save();
		if(!is_null($category)){
			session()->flash('success','category create Successfully!!');
			return redirect()->route('category_index');
		}else{
			session()->flash('stickly_error','Some Error Occer!!');
			return back();
		}
	}


	public function show($id)
	{
		$category=Category::findOrFail($id);
		return view('backend.admin.categories.show',compact('category'));
	}


	public function edit($id)
	{
		$category = Category::findOrFail($id);
		if(!is_null($category))
		{
			return view('backend.admin.categories.edit',[
				'category'=>$category
			]);
		}else
		{
			return redirect()->route('category_index');
		}
	}


	public function update(Request $request, $id)
	{

		$category=Category::findOrFail($id);
		$category->category_name=$request->category_name;
		$category->category_description=$request->category_description;
		if($request->file('category_image'))
        {
        	$images=$request->file('category_image');
            $img=$request->category_name.'.'.$images->getClientOriginalExtension();
            $location=public_path('assets/admin/images/categories/'.$img);
            Image::make($images)->save($location)->resize(300,300);
            $category->category_image=$img;
        }
		$category->save();
		if(!is_null($category)){
			session()->flash('success','category Update Successfully!!');
			return redirect()->route('category_index');
		}else{
			session()->flash('stickly_error','Some Error Occer!!');
			return back();
		}
	}
}
