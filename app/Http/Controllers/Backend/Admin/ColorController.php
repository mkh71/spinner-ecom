<?php

namespace App\Http\Controllers\Backend\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Color;

class ColorController extends Controller
{
    public function index()
	{
	    $colors =color::orderBy('id','desc')->get();
	    return view('backend.admin.colors.index',compact('colors'));
	}

	public function create()
	{
	    return view('backend.admin.colors.create');
	}

	public function store(Request $request)
	{
		$this->validate($request,[
			'color_name'=>'required'
			
		]);

		$color=new Color();
		$color->color_name=$request->color_name;
		$color->save();
		if(!is_null($color)){
			session()->flash('success','color create Successfully!!');
			return redirect()->route('color_index');
		}else{
			session()->flash('stickly_error','Some Error Occer!!');
			return back();
		}
	}


	public function show($id)
	{
		$color=Color::findOrFail($id);
		return view('backend.admin.colors.show',compact('color'));
	}


	public function edit($id)
	{
		$color = Color::findOrFail($id);
		if(!is_null($color))
		{
			return view('backend.admin.colors.edit',[
				'color'=>$color
			]);
		}else
		{
			return redirect()->route('color_index');
		}
	}


	public function update(Request $request, $id)
	{

		$color=Color::findOrFail($id);
		$color->color_name=$request->color_name;
		$color->save();
		if(!is_null($color)){
			session()->flash('success','color Update Successfully!!');
			return redirect()->route('color_index');
		}else{
			session()->flash('stickly_error','Some Error Occer!!');
			return back();
		}
	}
}
