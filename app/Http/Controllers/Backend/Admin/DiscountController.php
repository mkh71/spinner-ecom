<?php

namespace App\Http\Controllers\Backend\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Discount;
use App\Models\Product;

class DiscountController extends Controller
{
    public function index()
	{
	    $discounts =Discount::orderBy('id','desc')->get();
	    return view('backend.admin.discounts.index',compact('discounts'));
	}

	public function create()
	{
		 $products =Product::orderBy('id','desc')->get();
	    return view('backend.admin.discounts.create',[
	    	'products'=>$products
	    ]);
	}

	public function store(Request $request)
	{
		$this->validate($request,[
			'discount_code'=>'required',
			'discount_description'=>'required',
			'discount_type'=>'required',
			'product_id'=>'required',
			'discount_amount'=>'required',
			'discount_expire_date'=>'required',
			
		]);

		$discount=new Discount();
		$discount->discount_code=$request->discount_code;
		$discount->discount_description=$request->discount_description;
		$discount->discount_type=$request->discount_type;
		$discount->discount_expire_date=$request->discount_expire_date;
		$discount->discount_amount=$request->discount_amount;
		$discount->product_id=$request->product_id;
		$discount->save();
		if(!is_null($discount)){
			session()->flash('success','discount create Successfully!!');
			return redirect()->route('discount_index');
		}else{
			session()->flash('stickly_error','Some Error Occer!!');
			return back();
		}
	}


	public function show($id)
	{
		$discount=Discount::findOrFail($id);
		return view('backend.admin.discounts.show',compact('discount'));
	}


	public function edit($id)
	{
		$discount = Discount::findOrFail($id);
		if(!is_null($discount))
		{
			return view('backend.admin.discounts.edit',[
				'discount'=>$discount
			]);
		}else
		{
			return redirect()->route('discount_index');
		}
	}


	public function update(Request $request, $id)
	{

		$discount=Discount::findOrFail($id);
		$discount->discount_name=$request->discount_name;
		$discount->discount_description=$request->discount_description;
		if($request->file('discount_image'))
        {
        	$images=$request->file('discount_image');
            $img=$request->discount_name.'.'.$images->getClientOriginalExtension();
            $location=public_path('assets/admin/images/discounts/'.$img);
            Image::make($images)->save($location)->resize(300,300);
            $discount->discount_image=$img;
        }
		$discount->save();
		if(!is_null($discount)){
			session()->flash('success','discount Update Successfully!!');
			return redirect()->route('discount_index');
		}else{
			session()->flash('stickly_error','Some Error Occer!!');
			return back();
		}
	}
}
