<?php

namespace App\Http\Controllers\Backend\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Delivery;
class DeliveryController extends Controller
{
    public function index()
	{
	    $deliveries =Delivery::orderBy('id','desc')->get();
	    return view('backend.admin.deliveries.index',compact('deliveries'));
	}

	public function create()
	{
	    return view('backend.admin.deliveries.create');
	}

	public function store(Request $request)
	{
		$this->validate($request,[
			'delivery_name'=>'required',
			'delivery_description'=>'required',
			'delivery_amount'=>'required'
			
		]);

		$delivery=new Delivery();
		$delivery->delivery_name=$request->delivery_name;
		$delivery->delivery_description=$request->delivery_description;
		$delivery->delivery_amount=$request->delivery_amount;

		$delivery->save();
		if(!is_null($delivery)){
			session()->flash('success','delivery create Successfully!!');
			return redirect()->route('delivery_index');
		}else{
			session()->flash('stickly_error','Some Error Occer!!');
			return back();
		}
	}


	public function show($id)
	{
		$delivery=Delivery::findOrFail($id);
		return view('backend.admin.deliveries.show',compact('delivery'));
	}


	public function edit($id)
	{
		$delivery = Delivery::findOrFail($id);
		if(!is_null($delivery))
		{
			return view('backend.admin.deliveries.edit',[
				'delivery'=>$delivery
			]);
		}else
		{
			return redirect()->route('delivery_index');
		}
	}


	public function update(Request $request, $id)
	{

		$delivery=Delivery::findOrFail($id);
		$delivery->delivery_name=$request->delivery_name;
		$delivery->delivery_description=$request->delivery_description;
		
		$delivery->save();
		if(!is_null($delivery)){
			session()->flash('success','delivery Update Successfully!!');
			return redirect()->route('delivery_index');
		}else{
			session()->flash('stickly_error','Some Error Occer!!');
			return back();
		}
	}
}
