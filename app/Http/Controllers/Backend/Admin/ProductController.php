<?php

namespace App\Http\Controllers\Backend\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Brand;
use App\Models\Category;
use App\Models\ProductImage;
use App\Models\Color;
use App\Models\ProductColor;
use App\Models\Cartridge;
use Image;
use File;

class ProductController extends Controller
{
    public function index()
	{
	    $products =Product::orderBy('id','desc')->get();
	    return view('backend.admin.products.index',compact('products'));
	}

	public function create()
	{
        $cartridges =Cartridge::orderBy('id','desc')->get();
		$categories =Category::orderBy('id','desc')->get();
		$colors =Color::orderBy('id','desc')->get();
		$brands =Brand::orderBy('id','desc')->get();
	    return view('backend.admin.products.create',[
	    	'brands'=>$brands,
	    	'categories'=>$categories,
	    	'colors'=>$colors,
	    	'cartridges'=>$cartridges,
	    ]);
	}

	public function store(Request $request)
	{
		$this->validate($request,[
			'product_title'=>'required',

			'brand_id'=>'required',
			'category_id'=>'required',
			'cartridge_id'=>'required',

			'product_quantity'=>'required',
			'product_offer_price'=>'required',
			'product_description'=>'required'
			
		]);

		$product=new Product();
		$product->product_title=$request->product_title;
		$product->brand_id=$request->brand_id;
		$product->category_id=$request->category_id;
		$product->cartridge_id=$request->cartridge_id;

		$product->product_slug=str_slug($request->product_title);
		$product->product_quantity=$request->product_quantity;
		$product->product_offer_price=$request->product_offer_price;
		$product->product_description=$request->product_description;
		
		$product->save();
		$i=0;

		if(count($request->product_images)>0){
			foreach ($request->product_images as $image)
			{
				$i++;
			    $img=str_slug($request->product_title).'-'.$i.'.'.$image->getClientOriginalExtension();
			    $location=public_path('assets/admin/images/products/'.$img);
			    Image::make($image)->save($location)->resize(300,300);

			    $product_image=new ProductImage();
			    $product_image->product_image=$img;
			    $product_image->product_id=$product->id;
			    $product_image->save();
			}
		}


		if(!is_null($product)){
			session()->flash('success','product create Successfully!!');
			return redirect()->route('product_index');
		}else{
			session()->flash('stickly_error','Some Error Occer!!');
			return back();
		}
	}


	public function show($id)
	{
		$product=product::findOrFail($id);
		return view('backend.admin.products.show',compact('product'));
	}


	public function edit($id)
	{
		$product = product::findOrFail($id);
		if(!is_null($product))
		{
			return view('backend.admin.products.edit',[
				'product'=>$product
			]);
		}else
		{
			return redirect()->route('product_index');
		}
	}


	public function update(Request $request, $id)
	{

		$product=product::findOrFail($id);
		$product->product_name=$request->product_name;
		$product->product_description=$request->product_description;
		if($request->file('product_image'))
        {
        	$images=$request->file('product_image');
            $img=$request->product_name.'.'.$images->getClientOriginalExtension();
            $location=public_path('assets/admin/images/products/'.$img);
            Image::make($images)->save($location)->resize(300,300);
            $product->product_image=$img;
        }
		$product->save();
		if(!is_null($product)){
			session()->flash('success','product Update Successfully!!');
			return redirect()->route('product_index');
		}else{
			session()->flash('stickly_error','Some Error Occer!!');
			return back();
		}
	}
}
