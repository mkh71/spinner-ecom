<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Review;
use App\Models\Product;
use App\Models\ProductReview;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;

class ProductReviewController extends Controller
{
    public function show($id)
	{
		$product=Product::findOrFail($id);
		$reviews =Review::orderBy('id','desc')->get();
		return view('frontend.products.show',[
			'product'=>$product,
			'reviews'=>$reviews,
		]);
	}


	public function store(Request $request)
	{
		$this->validate($request,[
			'product_id'=>'required',
			'reviews'=>'required'
			
		]);
		$reviews= $request->reviews;
		foreach ($reviews as $review) {
			$product_review=new ProductReview();
			$product_review->product_id=$request->product_id;
			if(Auth::check()){
	            $product_review->user_id=Auth::id();
	        }
			$product_review->review_id=$review;
			$product_review->save();	
		}
		if(!is_null($product_review)){
			session()->flash('success','Brand create Successfully!!');
			return redirect()->route('/');
		}else{
			session()->flash('stickly_error','Some Error Occer!!');
			return back();
		}
	}



}
