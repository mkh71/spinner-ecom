<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use App\Models\Cart;

class CartController extends Controller
{

    public function index()
    {
        return view('frontend.carts.index');
    }

    
    public function store(Request $request)
    {
        $this->validate($request,[
            'product_id'=>'required',
        ]);

        if(Auth::check()){
            $carts=Cart::where('user_id',Auth::id())
            ->where('product_id',$request->product_id)
            ->where('order_id',NULL)
            ->first();

        }else
        {
            $carts=Cart::where('ip_address',request()->ip())
            ->where('product_id',$request->product_id)
            ->where('order_id',NULL)
            ->first();
            //dd($carts);
        }
        if (!is_null($carts)){
            $carts->increment('product_quantity');
        }else{
            $cart=new Cart();
            if(Auth::check()){
                $cart->user_id=Auth::id();
            }
            $cart->product_id=$request->product_id;
            $cart->ip_address=$request->ip();
            $cart->save();

            session::flash('success','cart Created Successfully');
            return back();
        }

            session::flash('success','cart not Created');
            return redirect()->back();
       
    }

    public function update(Request $request, $id)
    {
        $carts=Cart::findOrFail($id);
        if(!is_null($carts)){
            $carts->product_quantity=$request->product_quantity;
            $carts->update();
            return redirect('carts');
        }else{
            return redirect('carts');
        }
        return back();
    }
    public function delete($id)
    {
        $carts=Cart::findOrFail($id);
        if(!is_null($carts))
        {
            $carts->delete();
        }else{
            return redirect('carts');
        }
        session()->flash('success','Product has delete Successfully');
        return back();
    }
}
